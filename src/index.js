import React from 'react'
import {render} from 'react-dom'
import {Provider} from 'react-redux'
import store from './store'
import App from './components/layout'
import axios from 'axios';

axios.defaults.baseURL = 'https://my-json-server.typicode.com/aero-frontend/test-task/';

render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.querySelector('#root')
);