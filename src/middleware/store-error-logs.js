export const storeErrorLogs = store => next => action => {
    if (action.type.includes('FAIL')) {
        console.error(`Возникла ошибка в событии: ${action.type}, \n дополнительные данные: ${action.payload.message}`);
    }
    return next(action)
};