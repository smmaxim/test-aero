import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {getProducts, setProductFavorite} from '@/actions/products'
import ProductCard from '@/components/blocks/product/card'
import Loading from '@/components/ui/loading'
import './index.styl'

class ProductListFilter extends Component {

    componentDidMount() {
        this.props.getProducts()
    }

    onComparison(id) {
        console.log('add to comparison this id' + id);
    }

    render() {

        const {onComparison} = this;
        const {items, loading, favorites, setProductFavorite} = this.props;

        if (items.length === 0) {
            return <div className="text-center">ничего не найдено</div>
        }

        if (loading) {
            return <Loading/>
        }

        return (
            <div className="row">
                {!loading && items.map(item =>
                    <div className="col-4" key={item.id}>
                        <ProductCard
                            item={item}
                            onFavorite={id => setProductFavorite(id)}
                            onComparison={onComparison.bind(this)}
                            isFavotire={favorites.includes(item.id)}
                        />
                    </div>
                )}
            </div>
        )
    }

}

const mapStateToProps = ({products, filters}) => ({
    items: products.items,
    favorites: products.favorites,
    loading: products.loading,
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({getProducts, setProductFavorite}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductListFilter)