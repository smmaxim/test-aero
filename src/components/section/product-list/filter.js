import React, {Component} from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {getFilterProducts, getProducts} from '@/actions/products'
import ProductFilters, {ProductFiltersItem} from '@/components/blocks/product/filters'
import Checkbox from '@/components/ui/checkbox'
import './index.styl'

class ProductListFilter extends Component {

    state = {filters: []};

    onChange({value}) {
        const {filters} = this.state;
        const filterIndex = filters.indexOf(value);
        if (filterIndex === -1) filters.push(value);
        else filters.splice(filterIndex, 1);
        this.setState({filters});
    }

    onResetFilters() {
        this.setState({filters: []}, () =>
            this.props.getProducts()
        );
    }


    render() {

        const {filters} = this.state;
        const {brands, getFilterProducts, getProducts} = this.props;

        return (
            <ProductFilters
                onApply={() => filters.length > 0 ? getFilterProducts(filters) : alert('Фильтр не выбран')}
                onReset={this.onResetFilters.bind(this)}
            >

                <ProductFiltersItem title="Производитель">

                    <div className="row">
                        {brands && brands.map(({name, value}) =>
                            <div className="col-6" key={value}>
                                <Checkbox
                                    isChecked={filters.includes(value)}
                                    onChange={() => this.onChange({value})}>
                                    {name}
                                </Checkbox>
                            </div>
                        )}
                    </div>

                </ProductFiltersItem>

            </ProductFilters>
        )
    }

}

const mapStateToProps = ({products, filters}) => ({
    brands: filters.brands.items
})

const mapDispatchToProps = dispatch =>
    bindActionCreators({getProducts, getFilterProducts}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductListFilter)