import React, {Component} from 'react'
import ProductListItems from './items'
import ProductListFilter from './filter'
import './index.styl'

class ProductList extends Component {

    render() {

        return (
            <section className="product-list">

                <div className="container">

                    <div className="row">

                        <div className="col-9">
                            <ProductListItems/>
                        </div>

                        <div className="col-3">
                            <aside>
                                <ProductListFilter/>
                            </aside>
                        </div>

                    </div>

                </div>

            </section>
        )
    }

}

export default ProductList