import React from 'react'
import PropTypes from 'prop-types'
import './index.styl'


const Btn = ({children, theme, block, onClick, icon}) => (
    <button
        className={`ui-btn${theme ? ' ui-btn--' + theme : ''}${block ? ' ui-btn--block' : ''}${icon ? ' ui-btn--icon' : ''}`}
        onClick={onClick}>
        <span className="ui-btn__text">{children}</span>
    </button>
);

Btn.propTypes = {
    theme: PropTypes.string,
    block: PropTypes.bool,
    icon: PropTypes.bool
};

Btn.defaultProps = {
    block: false,
    icon: false
};

export default Btn