import React from 'react'
import {Icon} from '@/components/icons'
import './index.styl'

const Checkbox = ({children, isChecked, onChange}) => {
    return (
        <div className={`ui-checkbox${isChecked ? ' is-checked' : ''}`}>
            <label className="ui-checkbox__container">
                <input
                    className="ui-checkbox__control"
                    value={children}
                    checked={isChecked}
                    onChange={({target}) => onChange(!isChecked, children)}
                    type="checkbox"
                />
                <span className="ui-checkbox__box">
                    {isChecked && <Icon.check/>}
                </span>
                <span className="ui-checkbox__label">{children}</span>
            </label>
        </div>
    )
};

export default Checkbox