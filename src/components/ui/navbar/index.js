import React from 'react'
import PropTypes from 'prop-types'
import './index.styl'

const NavBar = ({items}) => {

    const List = ({items, className}) => (
        <ul className="navbar">
            {items.map(({id, name, url, items}) =>
                <li key={id} className="navbar__item">
                    <a href={url} className="navbar__link">{name}</a>
                    {items && <List items={items} className="has-children"/>}
                </li>
            )}
        </ul>
    );

    return (
        <nav className="navbar">
            <List items={items}/>
        </nav>
    )
};

NavBar.propTypes = {
    items: PropTypes.array
};

export default NavBar