import React from 'react'
import ProductList from '@/components/section/product-list/'
import Footer from './footer'
import ErrorBoundary from './errorBoundary'
import '@/assets/styl/scaffolding.styl'
import './index.styl'

const App = () => (
    <div className="app">
        <ErrorBoundary>

            <main className="app__content">
                <ProductList/>
            </main>

            <Footer/>

        </ErrorBoundary>
    </div>
);

export default App