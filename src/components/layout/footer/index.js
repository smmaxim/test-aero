import React from 'react'
import {connect} from 'react-redux'
import FooterNavbar from './navigation/'
import Phone from './phone/'
import Copyright from './copyright/'
import './index.styl'

const Footer = ({navigation, phone}) => (
    <footer className="footer">
        <div className="container">
            <div className="row justify-content-between">
                <div className="col-9">
                    <FooterNavbar items={navigation}/>
                    <Copyright/>
                </div>
                <div className="col-3">
                    <Phone number={phone}/>
                </div>
            </div>
        </div>
    </footer>
)

const mapStateToProps = ({navigation, contact}) => ({
    navigation: navigation.footer.items,
    phone: contact.phone
})
export default connect(mapStateToProps)(Footer)