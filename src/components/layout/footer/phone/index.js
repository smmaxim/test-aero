import React from 'react'
import './index.styl'

const FooterPhone = ({number}) => {

    const numberFormat = number => number.replace(/[-\s]/g, '')

    return (
        <div className="footer-phone">
            <div className="footer-phone__label">По любым вопросам обращайтесь:</div>
            <a className="footer-phone__number" href={`tel:${numberFormat(number)}`}>{number}</a>
        </div>
    )
}

export default FooterPhone