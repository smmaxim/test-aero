import React from 'react'
import PropTypes from 'prop-types'
import Navbar from '@/components/ui/navbar'
import './index.styl'

const FooterNavbar = ({items}) => (
    <Navbar items={items}/>
)

FooterNavbar.propTypes = {
    items: PropTypes.array
}

export default FooterNavbar