import React from 'react'
import './index.styl'

const Copyright = props => (
  <div className="copyright">
    © 2019, Интернет-магазин мебели
  </div>
)

export default Copyright