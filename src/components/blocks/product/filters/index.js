import React from "react";
import Btn from '@/components/ui/btn'
import './index.styl'

const ProductFilters = ({children, onApply, onReset}) => (
    <div className="product-filter box">

        <div className="product-filter__btns">
            <div className="product-filter__btn">
                <Btn theme="primary" block={true} onClick={onApply}>Показать результат</Btn>
            </div>
            <div className="product-filter__btn">
                <Btn block={true} onClick={onReset}>Очистить фильтр</Btn>
            </div>
        </div>

        <div className="product-filter__items">
            {children}
        </div>

    </div>
);

export const ProductFiltersItem = ({title, children}) => (
    <div className="product-filter-item">
        {title && <h5 className="product-filter-item__title">{title}</h5>}
        <div className="product-filter-item__body">
            {children}
        </div>
    </div>
);

export default ProductFilters