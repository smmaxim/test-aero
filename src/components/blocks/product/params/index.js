import React from 'react'
import PropTypes from 'prop-types'
import './index.styl'

const ProductParams = ({items}) => (
    <ul className="product-params">
        {items && items.map(({name, value}) =>
            <li className="product-params__item" key={value}>
                <label>{name}</label> {value}
            </li>
        )}
    </ul>
);


ProductParams.propTypes = {
    items: PropTypes.array
};

export default ProductParams