import React from 'react'
import PropTypes from 'prop-types'
import {Icon} from '@/components/icons/'
import './index.styl'


const ProductRaiting = ({raiting}) => (
    <div className="product-raiting">
        {Array.from(Array(5), (x, index) => index).map(item =>
            <span className={`product-raiting__item${raiting >= item + 1 ? ' is-active' : ''}`} key={item}>
                <Icon.star/>
            </span>
        )}
    </div>
);

ProductRaiting.propTypes = {
    raiting: PropTypes.number
};

export default ProductRaiting