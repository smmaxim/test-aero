import React from 'react'
import PropTypes from 'prop-types'
import './index.styl'

const ProductThumb = ({url, height}) => (
    <div className="product-thumb" style={{backgroundImage: url ? `url(${url})` : null, height}}/>
);

ProductThumb.propTypes = {
    url: PropTypes.string
};

ProductThumb.defaultProps = {
    height: 220
};

export default ProductThumb