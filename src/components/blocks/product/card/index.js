import React from 'react'
import PropTypes from 'prop-types'
import ProductParams from './../params/'
import ProductThumb from './../thumb/'
import ProductRaiting from './../raiting/'
import Btn from '@/components/ui/btn'
import {Icon} from '@/components/icons'
import './index.styl'


const ProductCard = ({item: {id, title, availability, params, code}, onFavorite, onComparison, isFavotire}) => (
    <div className="product-card box">

        <div className="product-card__top">

            <div className="row justify-content-between align-items-center">

                <div className="product-card__raiting">
                    <ProductRaiting raiting={3}/>
                </div>

                <div className="product-card__sky">
                    Арт. {code}
                </div>

            </div>

        </div>


        <div className="product-card__body">

            <div className="product-card__thumb">
                <ProductThumb/>
            </div>

            <div className={`product-card__availability${availability ? ' is-active' : ''}`}>
                <Icon.check/> {availability ? 'В наличии' : 'Нет в наличии'}
            </div>

            <h4 className="product-card__title">
                {title}
            </h4>

            <div className="product-card__params">
                <ProductParams items={params}/>
            </div>

            <div className="product-card__price">
                <div className="product-card__price-value">49 999 руб.</div>
                <div className="product-card__price-point">+490 бонусов</div>
            </div>

        </div>


        <div className="product-card__bottom">
            <div className="row justify-content-between align-items-center">
                <div className="col-6">
                    <Btn theme="primary" icon>
                        <Icon.cart/> Купить
                    </Btn>
                </div>
                <div className="col-6 text-right">
                    <div className={`product-card__favorite${isFavotire ? ' is-active' : ''}`}
                         onClick={() => onFavorite(id)}>
                        {isFavotire ? <Icon.favoriteFull/> : <Icon.favorite/>}
                    </div>
                    <div className="product-card__comparison" onClick={() => onComparison(id)}>
                        <Icon.comparison/>
                    </div>
                </div>
            </div>
        </div>

    </div>
);

ProductCard.propTypes = {
    item: PropTypes.object
};

export default ProductCard