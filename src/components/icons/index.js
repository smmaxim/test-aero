import React from "react";
import './index.styl';

export const Icon = {
    cart: () => (
        <i className="icon icon-ui icon-ui-cart">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 17 16">
                <g
                    fill="none"
                    fillRule="evenodd"
                    stroke="none"
                    strokeWidth="1"
                    transform="translate(-519 -402) translate(60 393) translate(444) translate(15 8)"
                >
                    <path d="M0 0H18V18H0z"/>
                    <circle cx="14.25" cy="15" r="1.5" fill="#FFF" fillRule="nonzero"/>
                    <circle cx="6.75" cy="15" r="1.5" fill="#FFF" fillRule="nonzero"/>
                    <path
                        fill="#FFF"
                        fillRule="nonzero"
                        d="M15.787 5.25l-11.25-.622L3.75 2.01A.75.75 0 003 1.5H.75a.75.75 0 000 1.5h1.71l.698 2.085 1.874 7.125a.75.75 0 00.795.465l10.02-2.085a.75.75 0 00.653-.75V6a.75.75 0 00-.713-.75z"
                    />
                </g>
            </svg>
        </i>
    ),

    check: () => (
        <i className="icon icon-ui icon-ui-check">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 8">
                <g
                    fill="none"
                    fillRule="evenodd"
                    stroke="none"
                    strokeWidth="1"
                    transform="translate(-87 -1086) translate(60 828) translate(25 254)"
                >
                    <path d="M0 0H16V16H0z"/>
                    <path
                        fillRule="nonzero"
                        d="M6 12a.667.667 0 01-.473-.193L2.86 9.14a.67.67 0 01.947-.947L6 10.393l6.193-6.2a.67.67 0 01.947.947l-6.667 6.667A.667.667 0 016 12z"
                    />
                </g>
            </svg>
        </i>
    ),

    comparison: () => (
        <i className="icon icon-ui icon-ui-comparsion">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <path
                    fillRule="nonzero"
                    stroke="none"
                    strokeWidth="1"
                    d="M19.916 12.185L16.553 6.58l3.019-.838a.585.585 0 10-.313-1.127l-3.752 1.042h-.001l-4.902 1.362V.585a.585.585 0 00-1.17 0v6.758L4.233 8.79.466 9.834a.585.585 0 00.313 1.127l2.347-.652-3.004 5.008a.585.585 0 00-.084.301 4.348 4.348 0 004.343 4.343 4.348 4.348 0 004.343-4.343.585.585 0 00-.083-.3L5.279 9.711 14.4 7.178l-3.004 5.007a.585.585 0 00-.083.301 4.348 4.348 0 004.343 4.343c1.16 0 2.25-.452 3.07-1.272A4.314 4.314 0 0020 12.487a.584.584 0 00-.084-.302zM4.382 18.791a3.178 3.178 0 01-3.119-2.588h6.238a3.178 3.178 0 01-3.119 2.588zm2.726-3.758H1.657l2.725-4.543 2.726 4.543zm8.549-7.674l2.725 4.542h-5.45l2.725-4.542zm0 8.3a3.178 3.178 0 01-3.119-2.588h6.238a3.178 3.178 0 01-3.12 2.589z"
                    transform="translate(-322 -1360) translate(60 828) translate(262 532)"
                />
            </svg>
        </i>
    ),

    favorite: () => (
        <i className="icon icon-ui icon-ui-favorite">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <path
                    fillRule="nonzero"
                    stroke="none"
                    strokeWidth="1"
                    d="M14.357.015c-1.702 0-3.296.771-4.357 2.063A5.643 5.643 0 005.643.015 5.649 5.649 0 000 5.658c0 2.436 1.453 5.254 4.318 8.375 2.205 2.402 4.603 4.262 5.286 4.775l.396.298.396-.298c.683-.513 3.081-2.372 5.286-4.775C18.547 10.913 20 8.094 20 5.658A5.649 5.649 0 0014.357.015zm.354 13.127c-1.814 1.975-3.765 3.573-4.711 4.309-.946-.736-2.897-2.334-4.71-4.31-2.598-2.83-3.971-5.417-3.971-7.483a4.329 4.329 0 014.324-4.324c1.569 0 3.017.855 3.78 2.232L10 4.606l.577-1.04a4.327 4.327 0 013.78-2.232 4.329 4.329 0 014.324 4.324c0 2.066-1.373 4.654-3.97 7.484z"
                    transform="translate(-549 -541) translate(327 9) translate(222 532)"
                />
            </svg>
        </i>
    ),

    favoriteFull: () => (
        <i className="icon icon-ui icon-ui-favorite-full">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                <path
                    fill="#0284C6"
                    fillRule="nonzero"
                    stroke="none"
                    strokeWidth="1"
                    d="M14.357.015c-1.702 0-3.296.771-4.357 2.063A5.643 5.643 0 005.643.015 5.649 5.649 0 000 5.658c0 2.436 1.453 5.254 4.318 8.375 2.205 2.402 4.603 4.262 5.286 4.775l.396.298.396-.298c.683-.513 3.081-2.372 5.286-4.775C18.547 10.913 20 8.094 20 5.658A5.649 5.649 0 0014.357.015z"
                    transform="translate(-599 -1360) translate(377 828) translate(222 532)"
                />
            </svg>
        </i>
    ),

    star: () => (
        <i className="icon icon-ui icon-ui-star">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10">
                <path
                    fillRule="evenodd"
                    stroke="none"
                    strokeWidth="1"
                    d="M5 8.03843137L8.09 10 7.2724902 6.30053664 10 3.81263158 6.4045098 3.48788442 5 0 3.5955098 3.48788442 0 3.81263158 2.7275098 6.30053664 1.91 10z"
                    transform="translate(-85 -848) translate(60 828) translate(25 20)"
                />
            </svg>
        </i>
    )
};