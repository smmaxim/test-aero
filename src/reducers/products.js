import * as types from "@/constants/ActionTypes"

const initialState = {
    favorites: [],
    items: [],
    loading: false
};

export default (state = initialState, action) => {
    switch (action.type) {

        // Get products
        case types.PRODUCTS_BEGIN:
            return {
                ...state,
                loading: true
            };

        case types.PRODUCTS_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.payload.products
            };

        case types.PRODUCTS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.data,
                items: []
            };

        // Add to favorite
        case types.PRODUCT_FAVORITE_SUCCESS:
            const {productID} = action.payload;
            const favorites = [...state.favorites];
            const favoriteIndex = favorites.indexOf(productID);
            if (favoriteIndex === -1) favorites.push(productID);
            else favorites.splice(favoriteIndex, 1);
            return {
                ...state,
                favorites
            };

        case types.PRODUCT_FAVORITE_FAILURE:
            return {
                ...state,
                error: action.payload.data
            };

        // Filter products
        case types.PRODUCTS_FILTER_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.payload.products
            };

        case types.PRODUCTS_FILTER_BEGIN:
            return {
                ...state,
                loading: true
            };

        case types.PRODUCTS_FILTER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.data
            };

        default:
            return state

    }
}