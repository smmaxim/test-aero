const initialState = {
    brands: {
        items: [
            {
                name: 'Canon',
                value: "canon"
            },
            {
                name: 'Olympus',
                value: "olympus"
            },
            {
                name: 'Fujifilm',
                value: "fujifilm"
            },
            {
                name: 'Pentax',
                value: "pentax"
            },
            {
                name: 'Nikon',
                value: "nikon"
            },
            {
                name: 'Sigma',
                value: "sigma"
            },
            {
                name: 'Panasonic',
                value: "panasonic"
            },
            {
                name: 'Geleral Electrics',
                value: "geleralElectrics"
            },
            {
                name: 'Leica',
                value: "leica"
            },
            {
                name: 'Zenit',
                value: "zenit"
            }
        ]
    }
};

export default (state = initialState) => {
    return state
}
