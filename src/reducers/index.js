import {combineReducers} from 'redux'
import navigation from './navigation'
import products from './products'
import filters from './filters'
import contact from './contact'

export default combineReducers({
    navigation,
    products,
    contact,
    filters
})
