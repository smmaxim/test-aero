const initialState = {
    header: {
        items: [
            {
                id: 0, name: 'Мебель', url: '#', items: [
                    {id: 6, name: 'Мягкая мебель', url: '#'},
                    {id: 7, name: 'Столы', url: '#'},
                    {id: 8, name: 'Стулья', url: '#'},
                    {id: 9, name: 'Стеллажи', url: '#'},
                    {id: 10, name: 'Шкафы-купе', url: '#'}
                ]
            },
            {id: 1, name: 'Хранение', url: '#'},
            {id: 2, name: 'Спальня', url: '#'},
            {id: 3, name: 'Кухня', url: '#'},
            {id: 4, name: 'Аксессуары', url: '#'},
            {id: 5, name: 'Акции', url: '#'}
        ]
    },
    footer: {
        items: [
            {id: 0, name: 'Каталог', url: '#'},
            {id: 1, name: 'Оплата', url: '#'},
            {id: 2, name: 'Доставка', url: '#'},
            {id: 3, name: 'Возврат', url: '#'},
            {id: 4, name: 'Сборка', url: '#'},
            {id: 5, name: 'Помощь', url: '#'},
            {id: 6, name: 'Обратная связь', url: '#'}
        ]
    }
};

export default (state = initialState) => {
    return state
}