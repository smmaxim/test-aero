import axios from 'axios'

import * as types from "@/constants/ActionTypes"

const initialState = {
    favorites: [],
    items: [],
    loading: false
};

export default (state = initialState, action) => {
    switch (action.type) {

        // Get products
        case types.PRODUCTS_BEGIN:
            return {
                ...state,
                loading: true
            };

        case types.PRODUCTS_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.payload.products
            };

        case types.PRODUCTS_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload.data,
                items: []
            };

        // Add to favorite
        case types.PRODUCT_FAVORITE_SUCCESS:
            const {productID} = action.payload;
            const favorites = [...state.favorites];
            const favoriteIndex = favorites.indexOf(productID);
            if (favoriteIndex === -1) favorites.push(productID);
            else favorites.splice(favoriteIndex, 1);
            return {
                ...state,
                favorites
            };

        case types.PRODUCT_FAVORITE_FAILURE:
            return {
                ...state,
                error: action.payload.data
            };

        // Filter products
        case types.PRODUCTS_FILTER_SUCCESS:
            return {
                ...state,
                loading: false,
                items: action.payload.products
            };

        case types.PRODUCTS_FILTER_BEGIN:
            return {
                ...state,
                loading: true
            };

        case types.PRODUCTS_FILTER_FAILURE:
            return {
                ...state,
                error: action.payload.data
            };

        default:
            return state
    }
}

export const getProductsSuccess = data => ({
    type: types.PRODUCTS_SUCCESS,
    payload: data
});

export const getProductsFailure = data => ({
    type: types.PRODUCTS_FAILURE,
    payload: {message: 'Ошибка загрузки'}
});

export const getFilterProductsSuccess = payload => ({
    type: types.PRODUCTS_FILTER_SUCCESS,
    payload
});

export const getFilterProductsFailure = payload => ({
    type: types.PRODUCTS_FILTER_FAILURE,
    payload
});

export const setProductFavoriteSuccess = payload => ({
    type: types.PRODUCT_FAVORITE_SUCCESS,
    payload
});

export const setProductFavoriteFailute = payload => ({
    type: types.PRODUCT_FAVORITE_FAILURE,
    payload
});

export const getProducts = () => {
    return dispatch => {
        dispatch({
            type: types.PRODUCTS_BEGIN
        });
        return axios.get('/PRODUCTS_SUCCESS')
            .then(result => {
                const {data, success} = result.data;
                if (success) dispatch(getProductsSuccess(data));
                else dispatch(getProductsFailure());
            })
    }
};

export const setProductFavorite = productID => {
    return dispatch => {
        return axios.get(`/FAVORITE_SUCCESS?productID=${productID}`, productID)
            .then(result => {
                const {data, success} = result.data;
                if (success) dispatch(setProductFavoriteSuccess({data, productID}));
                else dispatch(setProductFavoriteFailute(data));
            })
    }
};

export const getFilterProducts = array => {
    return dispatch => {
        dispatch({
            type: types.PRODUCTS_FILTER_BEGIN
        });
        return axios.get(`/FILTER_SUCCESS?filters=${array.join(',')}`, array)
            .then(result => {
                const {data, success} = result.data;
                if (success) dispatch(getFilterProductsSuccess(data));
                else dispatch(getFilterProductsFailure(data));
            })
    }
};