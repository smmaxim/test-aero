
## Get started

```bash
npm run dev - developer mode
npm run prod - production mode
```

Test project for Aero & E-pepper eCommerce Lab
React, Redux, Webpack, Stylus, BEM

Description:
https://docs.google.com/document/d/1iTIJd4c8mFpBDowAFbcImtxYL5hupBePKcQToqpw3YM/edit?usp=sharing

Source:
https://zpl.io/boMkBWk

Assets:
https://drive.google.com/drive/folders/1RZ-aIiN0QSg_xV9BLVS3uqGoG6I-hBmh?ogsrc=32
