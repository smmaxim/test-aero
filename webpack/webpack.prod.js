const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const poststylus = require('poststylus');
const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const webpackBaseConfig = require('./webpack.base');

const publicPath = path.join(__dirname, './../build/');

module.exports = merge(webpackBaseConfig, {
    output: {
        path: publicPath,
        filename: 'app.bundle.js',
        publicPath: '/'
    },
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin(),
            new OptimizeCSSPlugin({
                cssProcessorOptions: {
                    "preset": "advanced",
                    "safe": true,
                    "map": {"inline": false},
                },
            })
        ],
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            options: {
                stylus: {
                    use: [poststylus(['autoprefixer'])],
                    import: [
                        path.resolve(__dirname, '../src/assets/styl/variables.styl')
                    ]
                }
            }
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack.optimize.LimitChunkCountPlugin()
    ]
});