const webpack = require('webpack');
const merge = require('webpack-merge');
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

const webpackBaseConfig = require('./webpack.base');

module.exports = merge(webpackBaseConfig, {
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new HardSourceWebpackPlugin(),
        new HardSourceWebpackPlugin.ExcludeModulePlugin([
            {test: /mini-css-extract-plugin[\\/]dist[\\/]loader/}
        ])
    ]
});